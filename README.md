# Oops, No Lalafells!

A fork of [Oops, All Lalafells](https://ava.dev/ava/OopsAllLalafells) that intends to remove lalafells from FFXIV.

Demo: https://www.youtube.com/watch?v=NtugeWdCN6A

All credits for the original project goes to Ava Ryan.

### Installation (as a dev plugin)

- Install ffxiv, dalamud (through xivlauncher).
- Download [the latest build](https://fgl.ave.zone/pub/OopsNoLalafells.dll).
- Drop it into `%appdata%/XIVLauncher/devPlugins`.
- Launch FFXIV.

You can open the menu with `/ponl`.
